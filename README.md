
# Custom HL7 Java library

> **_Note:_** A local JDK 8 installation is required for this step

For details of configuration customizations please refer to the online documentation at
https://odin-doc.atlassian.net/wiki/spaces/DOC4X/pages/2752555/HL7+Custom+Message

To build this project on a Windows environment, execute

`mvnw.cmd clean package`

or on a Linux/OSX environment

`./mvnw clean package`

If build succeeds, jar file hl7-custom-structure-1.0.0-SNAPSHOT.jar can be found in the target directory